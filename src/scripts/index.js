import '../styles/index.scss';

$(document).ready(()=>{
    
    console.log('Hello jQuery');

    $(function(){
        $('.bxslider').bxSlider({
          auto: true,
          pager:false
        });
      });

    

    $('.accordion').click(function(){
        $('.panel').slideUp();
        $('.arrow').addClass('arrowDown').removeClass('arrowUp');
        if ($(this).next().is(':not(:visible)')){
            $(this).next().slideDown();
            $(this).find('.arrow').addClass('arrowUp').removeClass('arrowDown');
        }
    });

    /**
     * Documentation for the slider
     * https://bxslider.com/examples/auto-show-start-stop-controls/ 
     */ 

     /**
     * Documentation for the Lightbox - Fancybox
     * See the section 5. Fire plugin using jQuery selector.
     * http://fancybox.net/howto
     */ 

     /**
      * Boostrap Modal (For the Learn More button)
      * https://getbootstrap.com/docs/4.0/components/modal/
      */
    

});

/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */ 
window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};
